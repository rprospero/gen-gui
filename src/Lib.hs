{-# LANGUAGE TypeOperators, DeriveGeneric, FlexibleContexts, FlexibleInstances, TypeSynonymInstances, ScopedTypeVariables, DefaultSignatures #-}
module Lib
    ( someFunc
    ) where

import Control.Monad (void)
import GHC.Generics
import qualified Graphics.UI.Threepenny as UI
import Graphics.UI.Threepenny.Core
import Safe (readMay)
  
class Viewable a where
  getView :: a -> UI (Element, Behavior a)
  
  default getView :: (Generic a, GViewable (Rep a)) => a -> UI (Element, Behavior a)
  getView a = do
    (el, x) <- getView_ $ from a
    return (el, to <$> x)

class GViewable f where
  getView_ :: f a -> UI (Element, Behavior (f a))

instance Viewable String where
  getView base = do
    box <- UI.input
    boxIn <- stepper base $ UI.valueChange box
    return (box, boxIn)

instance Viewable Int where
  getView base = do
    box <- UI.input
    boxIn <- stepper base . filterJust . fmap readMay $ UI.valueChange box
    return (box, boxIn)

instance Viewable Double where
  getView base = do
    box <- UI.input
    boxIn <- stepper base . filterJust . fmap readMay $ UI.valueChange box
    return (box, boxIn)

instance Viewable Bool where
  getView base = do
    box <- UI.input
    element box # set (attr "type") "checkbox"
    boxIn <- stepper base $ UI.checkedChange box
    return (box, boxIn)

instance GViewable U1 where
  getView_ U1 = do
    box <- UI.new
    return (box, pure U1)

-- instance (GViewable a) => GViewable (M1 i c a) where
--   getView_ = do
--     (el, val) <- getView_
--     return (el, M1 <$> val)


instance (GViewable a, Datatype c) => GViewable (D1 c a) where
  getView_ full@(M1 x) = do
    wrapper <- UI.new
    (el, val) <- getView_ x
    element wrapper #+ [element el]
    element wrapper # set UI.class_ ((datatypeName full) <> "_type")
    return (wrapper, M1 <$> val)
    
instance (GViewable a, Constructor c) => GViewable (C1 c a) where
  getView_ full@(M1 x)= do
    (el, val) <- getView_ x
    element el # set UI.class_ ((conName full) <> "_con")
    return (el, M1 <$> val)
    
instance (GViewable a, Selector c) => GViewable (S1 c a) where
  getView_ full@(M1 x) = do
    (el, val) <- getView_ x
    tagged <- row [string (selName full), element el]
    element tagged # set UI.class_ (selName full)
    return (tagged, M1 <$> val)
    
instance (GViewable a, GViewable b) => GViewable (a :*: b) where
  getView_ (ina :*: inb) = do
    (ela, vala) <- getView_ ina
    (elb, valb) <- getView_ inb
    
    widget <- column [element ela, element elb]
    return (widget, (:*:) <$> vala <*> valb)

instance (Viewable a) => GViewable (K1 i a) where
  getView_ (K1 x) = do
    (el, val) <- getView x
    return (el, K1 <$> val)

-- instance Viewable Example where
  -- getView = do
    -- (dollar, dollarIn) <- getView
    -- (euro, euroIn) <- getView

    -- let combined = Example <$> dollarIn <*> euroIn
    -- widget <- column [element dollar, element euro]
    -- return (widget, combined)

data Example = Example {
  count :: Int,
  name :: String,
  size :: Bool
  } deriving (Read, Show, Generic)
  
instance Viewable Example
  
someFunc :: IO ()
someFunc = startGUI defaultConfig{jsStatic = Just "./static"} setup

setup :: Window -> UI ()
setup window = void $ do
  return window # set title "Example"
  UI.addStyleSheet window "style.css"

  (widgets, combined :: Behavior Example) <- getView $ Example 1 "base" False
  
  message <- UI.new
  
  getBody window #+ [element widgets, element message ]
  
  element message # sink text (("Result: " <>) . show <$> combined)
  -- getBody window #+ [ string "Example" ]